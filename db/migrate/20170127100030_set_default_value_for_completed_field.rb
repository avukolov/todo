class SetDefaultValueForCompletedField < ActiveRecord::Migration[5.0]
  def self.up
   change_column :tasks, :completed, :boolean, :default => false
 end
end
