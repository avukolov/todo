# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


rand(7..17).times do
  Project.create(title: "#{Forgery('address').city} #{Forgery('basic').color} ")
  rand(5..40).times do
    Project.last.tasks.create(name: "#{Forgery('internet').user_name} #{Forgery('address').street_address}", deadline: Forgery('date').date.strftime('%Y-%m-%d'), completed: Forgery('basic').boolean)
  end
end
