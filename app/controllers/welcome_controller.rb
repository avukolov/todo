class WelcomeController < ApplicationController
  skip_before_action :authenticate_user!
  respond_to :html, :js

  def index

  end

end
