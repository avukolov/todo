class TasksController < ApplicationController
  before_action :set_project
  before_action :set_task, except: [:create]

  def create
    @task = @project.tasks.create(task_params)
    respond_to do |format|
      if @task.save
        format.js {}
        format.json { render partial: "task", status: :ok, locals: {task: @task}, location: @task }
      else
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  def completed
    respond_to do |format|
      if params[:completed] == "close"
        @task.update(completed: true) && @task.remove_from_list
        format.js {}
        format.json { render partial: "project", status: :ok, locals: {project: @project} }
      elsif params[:completed] == "open"
        @task.update(completed: false) && @task.insert_at(@project.tasks.uncompleted.count + 1)
        format.js {}
        format.json { render partial: "project", status: :ok, locals: {project: @project} }
      end
    end
  end

  def change_position
    respond_to do |format|
      if params[:direction] == "up"
        @task.move_higher
        format.js {}
        format.json { render partial: "project", status: :ok, locals: {project: @project} }
      elsif params[:direction] == "down"
        @task.move_lower
        format.js {}
        format.json { render partial: "project", status: :ok, locals: {project: @project} }
      end
    end
  end

  def destroy
    if @task.destroy
      flash[:success] = "Task was deleted."
      respond_to do |format|
          format.js {}
      end
    else
      flash[:error] = "Task could not be deleted."
    end
  end

  def update
    respond_to do |format|
      if @task.update(task_params)
        format.js {}
        format.json { render partial: "task", data: {task_id: @task.id}, status: :ok, locals: {task: @task}, location: @task }
      else
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

private
  def set_project
    @project = current_user.projects.find(params[:project_id])
  end

  def set_task
    @task = @project.tasks.find(params[:id])
  end

  def get_last_position
    @last_position = @project.tasks.where.not(position: nil).last.position
  end

  def task_params
    params.require(:task).permit(:name, :deadline, :completed, :position)
  end
end
