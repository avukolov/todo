class Task < ApplicationRecord
  belongs_to :project
  acts_as_list scope: :project
  scope :uncompleted, -> { where(completed: false) }
  scope :completed, -> { where(completed: true) }

  validates :name,
            :presence => {:message => "can't be blank." }
  validate :deadline_cannot_be_in_the_past

  def deadline_cannot_be_in_the_past
    if !deadline.blank? and deadline < Date.today
      errors.add(:deadline, "date can't be in the past")
    end
  end

end
