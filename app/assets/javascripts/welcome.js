$(function() {
  return $("form#sign_in_user, form#sign_up_user").on("ajax:success", function(event, xhr, settings) {
    $(this).parents('.modal').modal('hide');
    location.reload();
  }).on("ajax:error", function(event, xhr, settings, exceptions) {
    var error_messages;
    deleteAlerts();
    error_messages = xhr.responseJSON['error'] ? "<div class='alert alert-danger pull-left'>" + xhr.responseJSON['error'] + "</div>" : xhr.responseJSON['errors'] ? $.map(xhr.responseJSON["errors"], function(v, k) {
      return "<div class='alert alert-danger pull-left'>" + k + " " + v + "</div>";
    }).join("") : "<div class='alert alert-danger pull-left'>Unknown error</div>";
    return $('.modal-footer').append(error_messages);
  });
});


function deleteAlerts() {
  if (document.getElementsByClassName('alert').length > 0) {
      $('.alert').remove();
  }
}
