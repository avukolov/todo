// Completed method for tasks
var initAjaxCompleted  = function(){
  $('.task-check').on('click', function() {
    if ($(this).prop('checked') === true) {
      $.ajax({
        url: '/projects/'+ this.value +'/tasks/'+ this.id +'/completed',
        type: 'PATCH',
        data: {"completed": "close"}
        });
    } else if ($(this).prop('checked') === false) {    
      $.ajax({
        url: '/projects/'+ this.value +'/tasks/'+ this.id +'/completed',
        type: 'PATCH',
        data: {"completed": "open"}
        });
    }
  });
};
