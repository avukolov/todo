Rails.application.routes.draw do
  devise_for :users, controllers: {sessions: 'sessions', registrations: 'registrations'}

  resources :projects do
    resources :tasks do
      member do
        patch :completed
        patch :change_position
      end
    end
  end

  root "welcome#index"
end
